package com.bawei.struts2.actions;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class MyInterceptor implements Interceptor{

	private String a;
	
//	@Override
	public void destroy() {
	}

//	@Override
	public void init() {
//		System.out.println("init");
//		String a = ServletActionContext.getServletContext().getInitParameter("a");
//		System.out.println("a:"+a);
	}

//	@Override
	public String intercept(ActionInvocation invoke) throws Exception {
		
		System.out.println("a==="+a);
		
		System.out.println("MyInterceptor start ... ");
		
		String requst = invoke.invoke();
		
		System.out.println("MyInterceptor end ... ");
		
		return requst;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	
}
