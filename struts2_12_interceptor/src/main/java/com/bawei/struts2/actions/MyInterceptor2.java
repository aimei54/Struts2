package com.bawei.struts2.actions;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class MyInterceptor2 extends AbstractInterceptor {
	    
	private static final long serialVersionUID = 3360144694054829421L;

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {
		System.out.println("MyInterceptor2 start ... ");
		
		String resut = arg0.invoke();
		
		System.out.println("MyInterceptor2 end ... ");
		return resut;
	}

}
