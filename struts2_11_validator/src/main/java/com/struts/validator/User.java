package com.struts.validator;

public class User {
	private int age;
	
	private String password1;
	private String password2;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	@Override
	public String toString() {
		return "User [age=" + age + ", password1=" + password1 + ", password2=" + password2 + "]";
	}
	
}
