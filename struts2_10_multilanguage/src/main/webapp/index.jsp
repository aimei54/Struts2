<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<!-- 
	AA:<s:text name="name"></s:text><br>
	
	BB:<s:text name="user.name"></s:text><br>
 -->	

	<s:form>
		<s:textfield key="user.name" name="name"></s:textfield>
		<s:textfield key="user.age" name="age"></s:textfield>
		<s:submit key="sumbit"></s:submit>
	</s:form>
	
	<a href="test?request_locale=zh_CN">中文</a>|
	<a href="test?request_locale=en_US">English</a>
	
	<a href="input">进入</a>
</body>
</html>