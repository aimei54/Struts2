<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.bawei.struts2.actions.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<%
		request.setAttribute("sess", "request");
		request.setAttribute("age", 23);
		User u = new User();
		
		u.setName("lisi");
		request.setAttribute("u1", u);
		List<String> l = new ArrayList<String>();
		l.add("111");
		l.add("222");
		l.add("333");
		
		request.setAttribute("l", l);
	%>
	
	<!-- s:debug : 调试使用的标签，可以看到值栈 -->
	<s:debug></s:debug>
	
	<!-- s:property : 取值 -->
	name: <s:property value="#request.u1.name"/>
	
	<!-- s:set ： 设置属性，把ui设置给u2 -->
	<s:set value="#request.u1" var="u2" scope="session" >
	</s:set>
	
	name2: <s:property value="#session.u2.name" />
	
	<!-- s:url : 构造一个连接，下面的会这个会形成/应用名/user?pwd=111的字符串，如果没有id，会在页面显示这个字符串，有了id就不再显示了 -->
	url:<s:url action="user" id="test">
		<!-- 可以配置参数 -->
		<s:param name="pwd">111</s:param>
	</s:url>
	
	<!-- s:a 创建一个超链接，可以使用已经创建好的url.比喻%{test} -->
	<s:a href="%{test}">连接</s:a>
	
	<!-- s:a 标签的另外一种写法，相当于把url和上面的a标签写法融合在一起了 -->
	<s:a action="user" 
		namespace="/"
	>连接2
		<s:param name="pwd">2222</s:param>
	</s:a>
	
	<s:property value="[1].name"/><br>
	reques:<s:property value="#request.name"/><br>
	session:<s:property value="#session.name"/><br>
	<!-- 
	<s:property value="user.name"/>|<s:property value="user.age"/><br>
	<s:property value="request.user.name"/>|<s:property value="request.user.age"/><br>
	<s:property value="#request.user.name"/>|<s:property value="#request.user.age"/><br>
	<s:property value="#request.user['name']"/>|<s:property value="#request.user['age']"/><br>
	 -->
	 
	 <!-- s:if,s:elseif,s:else : 控制标签 ，类似于Java的条件语句-->
	<s:if test="#request.age==23">
		等于23
	</s:if>
	<s:elseif test="#request.age==25">
		
	</s:elseif>
	<s:else>
		
	</s:else>
	
	<!-- s:iterator: 迭代标签 -->
	<s:iterator value="#request.l" var="str" id="strr">
		<s:property value="#strr"/><br>
	</s:iterator>
	
</body>
</html>