<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<sx:head/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<%
		Map<String,String> hos = new HashMap<String,String>();
		hos.put("111", "打球");
		hos.put("222", "泡妹子");
		hos.put("333", "上网");
		request.setAttribute("hobs", hos);
	%>
	
	<!-- 页面展示标签：
		表单标签，
	 -->
	<s:form action="user" method="post">
		<!-- 
			label:提示
			name ： 提交或者回显时候字段名
		 -->
		<!-- 文本框 -->
		<s:textfield label="用户名" name="name"></s:textfield>
		<!-- 单选框，list用来指定各个选项，是键值对的格式 -->
		<s:radio label="性别" name="gender" list="#{'1':'男','2':'女'}"></s:radio>
		<!-- 复选框 -->
		<s:checkbox label="是否结婚" name="married"></s:checkbox>
		
		<!-- 组合复选框
			list，是用来组合选项的，
			构造形式：
			①.井{'key1':'val1','key2':'val2'...}，其实就是一个map
			②.可以是一个对象集合，listKey来指定用来设置value的这个属性，listValue是要显示出来的属性名
		 -->
		 <!-- 
		<s:checkboxlist label="兴趣爱好" name="hobby" list="#request.hobbies"  listKey="hobbyId" listValue="hobbyName">
		</s:checkboxlist>
		 -->
		
		<!-- 组合复选框 -->
		<s:checkboxlist label="兴趣爱好" name="hobby" list="#request.hobs" >
		</s:checkboxlist>
		
		<!-- 下拉框 ，赋值类似于咱们的checkboxlist
			list的内容可以是map类型的，也可以是对象集合的。
			listKey ： 标识对象的id属性
			listValue ： 显示出来的属性
			headerKey ： 
			headerValue ：最上面的，一般用于提示
		-->
		<s:select label="所在城市" name="city" list="#request.cities" listKey="cityId" listValue="cityName"
			 headerKey="03" headerValue="请选择城市"
		></s:select>
		<s:file label="请选择头像" name="photo"></s:file>
		<!-- 日历控件 ，需要引入struts2-dojo-plugin-2.3.15.3.jar，需要在JSP头部引入标签，并且声明sx:head-->
		<sx:datetimepicker name="datea" label="时间" displayFormat="dd-MM-yyyy"
			value="%{'2010-01-01'}"></sx:datetimepicker>
		<!-- 文本域 -->
		<s:textarea label="简介" name="desc" cols="30" rows="5">
		</s:textarea>
		<s:submit value="提交"></s:submit>
		<s:reset value="重置"></s:reset>
	</s:form>
	
</body>
</html>