package com.bawei.struts2.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.RequestAware;

public class InputAction implements RequestAware {

	private Map<String, Object> requestMap;

	public String input() {

		List<Hobby> hobbies = new ArrayList<Hobby>();
		hobbies.add(new Hobby("1001", "上网"));
		hobbies.add(new Hobby("1002", "撩妹"));
		hobbies.add(new Hobby("1003", "玩游戏"));
		requestMap.put("hobbies", hobbies);

		List<City> cities = new ArrayList<City>();
		cities.add(new City("01", "北京"));
		cities.add(new City("02", "武汉"));
		cities.add(new City("03", "十堰"));
		requestMap.put("cities", cities);

		return "input";
	}

//	@Override
	public void setRequest(Map<String, Object> arg0) {
		this.requestMap = arg0;
	}

}
