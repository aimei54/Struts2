package com.bawei.struts2.actions;

import com.opensymphony.xwork2.ActionContext;

public class UserAction {

	private User user;
	
	private String name;
	
	public String execute(){
		user = new User();
		user.setName("zhangsan");
		ActionContext.getContext().getValueStack().push(user);
		return "success";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public User getUser() {
//		return user;
//	}
//
//
//	public void setUser(User user) {
//		this.user = user;
//	}

}