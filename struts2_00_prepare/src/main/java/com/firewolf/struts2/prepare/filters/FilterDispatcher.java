package com.firewolf.struts2.prepare.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 使用Filter来作为控制器，通过web.xml中配置来让请求转发到Filter中，然后使用Filter来接受参数并处理逻辑
 * @Author: 刘兴
 * @Date : 2016年8月15日
 * @package : com.firewolf.struts2.prepare.filters
 * @Java_Version : 1.7
 * @Version : 2016年8月15日下午9:35:33
 */
public class FilterDispatcher implements Filter {

	public void destroy() {}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//所有的action请求都会转发到这里，然后可以在下面进行处理
		HttpServletRequest req = (HttpServletRequest) request;
		//1. 获取 servletPath
		String servletPath = req.getServletPath();
		String path = null;	
		//2. 判断 servletPath, 若其等于 "/product-input.action", 则转发到
		///WEB-INF/pages/input.jsp
		if("/product-input.action".equals(servletPath)){
			path = "/WEB-INF/pages/input.jsp";
		}
		//3. 若其等于 "/product-save.action", 则
		if("/product-save.action".equals(servletPath)){
			//1). 获取请求参数
			String productName = request.getParameter("productName");
			String productDesc = request.getParameter("productDesc");
			String productPrice = request.getParameter("productPrice");
			//2). 把请求信息封装为一个 Product 对象
			Product product = new Product(null, productName, productDesc, Double.parseDouble(productPrice));		
			//3). 执行保存操作
			System.out.println("Save Product: " + product);
			product.setProductId(1001);		
			//4). 把 Product 对象保存到 request 中. ${param.productName} -> ${requestScope.product.productName}
			request.setAttribute("product", product);		
			path = "/WEB-INF/pages/details.jsp";
		}
		if(path != null){
			request.getRequestDispatcher(path).forward(request, response);
			return;
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {}

}
