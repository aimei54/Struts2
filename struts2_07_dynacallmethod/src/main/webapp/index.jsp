<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<!-- 
		这个action配置的方法是save
		http://localhost:8080/me_struts2_05_commonmatch/user!update.do
		这种形式可以访问到update方法
	 -->
	<a href="user.action">User Save</a><br>
	<a href="user!update.action">User Update</a><br>
	<a href="user!delete.action">User Delete</a><br>
	<a href="user!query.action">User Query</a><br>
</body>
</html>