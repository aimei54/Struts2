package com.firewolf.struts2.actions;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@ParentPackage("struts-default")
@Namespace("/")
@Results({
	@Result(name="success",location="/wel.jsp"),
	@Result(name="input",location="/input.jsp"),
	@Result(name="err",location="/err.jsp"),
		})
//@Actions(
//		value = {  
//	    @Action(value = "foreignship",  results = { 
//	    		@Result(name = "success", location = "foreignship/foreignship.jsp") 
//	    		}),  
//	    @Action(value = "foreignshipsee", results = { 
//	    		@Result(name = "success", location = "foreignship/foreignshipsee.jsp") 
//	    		}) 
//	    }) 
public class LoginAction {

	private String name;
	
	@Action(value="toInput")
	public String toInput(){
		return "input";
	}
	
	@Action(value="test" ,results={@Result(name="test",location="/test.jsp")})
	public String test(){
		return "test";
	}
	
	@Action(value="login")
	public String login(){
		System.out.println("name:"+name);
		return "success";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
