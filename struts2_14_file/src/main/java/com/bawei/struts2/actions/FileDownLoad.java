package com.bawei.struts2.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class FileDownLoad extends ActionSupport {

	private String userid;
	
	private String fileName;
	
	private String contentDisposition;

	private String contentType;

	private Long contentLength;

	private InputStream is;

	@Override
	public String execute() throws Exception {
		
		//用userid查用户名
		
		//再构建真正的文件路径
		
		ServletContext servletContext = ServletActionContext.getServletContext();
		String fileName = servletContext.getRealPath("/a.txt");
		//创建输入流
		File file = new File(fileName);
		is = new FileInputStream(fileName);
	
		contentType = "text/plain";
		contentLength = (long) is.available();
		contentDisposition = "attachment;filename=" + "a.txt";
		return SUCCESS;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getContentLength() {
		return contentLength;
	}

	public void setContentLength(Long contentLength) {
		this.contentLength = contentLength;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
