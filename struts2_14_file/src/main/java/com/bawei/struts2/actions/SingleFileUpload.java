package com.bawei.struts2.actions;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件上传action，需要继承ActionSupport类
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月12日
 * @package : com.bawei.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月12日下午1:49:28
 */
public class SingleFileUpload extends ActionSupport{
	
	    
	private static final long serialVersionUID = 4494142878725644188L;

	//和前台的file的name属性值一样
	private File myfile;
	
	//文件名，格式：${myfile}FileName,后面的这个FileName是固定的
	private String myfileFileName;
	
	//文件类型，格式：${myfile}ContentType,ContentType是固定不能变的
	private String myfileContentType;

	@Override
	public String execute() throws Exception {
		System.out.println(myfileFileName+","+myfileContentType);
		
		String folder = ServletActionContext.getServletContext().getRealPath("/files")+"\\";
		
		
		String realName = folder + myfileFileName;
		
		System.out.println("realName:"+realName);
		
		FileUtils.copyFile(myfile, new File(realName));
		
		return super.execute();
	}
	
	public File getMyfile() {
		return myfile;
	}

	public void setMyfile(File myfile) {
		this.myfile = myfile;
	}

	public String getMyfileFileName() {
		return myfileFileName;
	}

	public void setMyfileFileName(String myfileFileName) {
		this.myfileFileName = myfileFileName;
	}

	public String getMyfileContentType() {
		return myfileContentType;
	}

	public void setMyfileContentType(String myfileContentType) {
		this.myfileContentType = myfileContentType;
	}
	
	
}
