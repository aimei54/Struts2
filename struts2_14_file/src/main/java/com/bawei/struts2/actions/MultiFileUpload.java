package com.bawei.struts2.actions;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 文件上传action，需要继承ActionSupport类
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月12日
 * @package : com.bawei.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月12日下午1:49:28
 */
public class MultiFileUpload extends ActionSupport{
	
	    
	private static final long serialVersionUID = 4494142878725644188L;

	//和前台的file的name属性值一样
	private List<File> myfile;
	
	//文件名，格式：${myfile}FileName,后面的这个FileName是固定的
	private List<String> myfileFileName;
	
	//文件类型，格式：${myfile}ContentType,ContentType是固定不能变的
	private List<String> myfileContentType;

	@Override
	public String execute() throws Exception {
		String folder = ServletActionContext.getServletContext().getRealPath("/files")+"\\";
		if(myfile != null && myfile.size() > 0){
			for(int i = 0 ;i<myfile.size();i++){
				System.out.println(myfileFileName.get(i)+","+myfileContentType.get(i));
				String realName = folder + myfileFileName.get(i);
				FileUtils.copyFile(myfile.get(i), new File(realName));
			}
		}
		return super.execute();
	}

	public List<File> getMyfile() {
		return myfile;
	}

	public void setMyfile(List<File> myfile) {
		this.myfile = myfile;
	}

	public List<String> getMyfileFileName() {
		return myfileFileName;
	}

	public void setMyfileFileName(List<String> myfileFileName) {
		this.myfileFileName = myfileFileName;
	}

	public List<String> getMyfileContentType() {
		return myfileContentType;
	}

	public void setMyfileContentType(List<String> myfileContentType) {
		this.myfileContentType = myfileContentType;
	}
	
	
}
