<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 上传文件的时候注意编码：enctype="multipart/form-data"  -->	
	<s:form action="fileupload" method="post" enctype="multipart/form-data">
		<s:file label="头像" name="myfile"></s:file>
		<s:submit value="提交"></s:submit>
	
	</s:form>
	
	<hr>
	
	<!-- 多文件，前台表单中有多个文件输入框，他们的name属性是一样的-->
	<s:form action="fileupload2" method="post" enctype="multipart/form-data">
		<s:file label="头像" name="myfile"></s:file>
		<s:file label="头像2" name="myfile"></s:file>
		<s:file label="头像3" name="myfile"></s:file>
		<s:submit value="提交"></s:submit>
	</s:form>
	 
	
	<hr>
	<a href="download?fileName=a.txt">a.txt</a>
	
	
</body>
</html>