package com.bawei.struts2.actions;
/**
 * @Description:
 	基于属性驱动的方式接受参数（属性是JavaBean）
 		这种方式要求前台表单中以user.name【即对象名.属性名】的形式来写参数的name属性，
 * @Author: 刘兴
 * @Date : 2016年9月21日
 * @package : com.bawei.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月21日下午11:01:48
 */
public class UserAction2 {
	
	//定义一个对象用来接收数据，注意需要生成setter和getter方法
	private User user;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String execute(){
		System.out.println("UserAction2: "+user);
		return "success";
	}

}
