package com.bawei.struts2.actions;

import com.opensymphony.xwork2.ModelDriven;
/**
 * 基于模型驱动的Action
 	
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月21日
 * @package : com.bawei.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月21日下午11:11:23
 */
public class UserAction3 implements ModelDriven<User>{
	
	/**
	 * 需要定义一个用来接受数据的模型，
	 */
	private User user = new User();

	public String execute(){
		System.out.println("UserAction3: "+user);
		return "success";
	}

	/**
	 * 要返回这个模型对象
	 */
	public User getModel() {
		return user;
	}
	
}
