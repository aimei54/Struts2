package com.bawei.struts2.actions;
/**
 * @Description: 基于属性驱动的方式接受参数
 	在对象中定义普通属性，前台表单中写上属性
 	
 * @Author: 刘兴
 * @Date : 2016年9月21日
 * @package : com.bawei.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月21日下午10:42:01
 */
public class UserAction {

	//定义两个属性用来接收数据，注意需要提供setter和getter方法
	private String name;
	
	private String pwd;

	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	@Override
	public String toString() {
		return "UserAction [name=" + name + ", pwd=" + pwd + "]";
	}


	public String execute(){
		System.out.println(this);
		return "success";
	}
}
