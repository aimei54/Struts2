<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script type="text/javascript" src="jquery-1.11.3.min.js"></script>
<script type="text/javascript">
	$(function() {
	
		$("#btn").click(function(){
			$.ajax({
				url : "data-getJson",
				data : {
				},
				async : true,
				dataType : 'json',
				type : 'post',
				success : function(data) {
					console.log(data);
				}
			})
		});
		$("#btn1").click(function(){
			$.ajax({
				url : "data-getJson1",
				data : {
				},
				async : true,
				dataType : 'json',
				type : 'post',
				success : function(data) {
					console.log(data);
				}
			})
		});
	
	})
</script>
</head>
<body>
	<button id="btn">使用传统方式获取Json数据</button><br>
	<button id="btn1">使用struts功能获取Json数据</button>
</body>
</html>