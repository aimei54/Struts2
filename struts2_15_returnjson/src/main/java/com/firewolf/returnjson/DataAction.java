package com.firewolf.returnjson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.JSONUtil;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @Description: Action，要返回Json数据
 * @Author: 刘兴
 * @Date : 2016年10月8日
 * @package : com.firewolf.returnjson
 * @Java_Version : 1.7
 * @Version : 2016年10月8日上午8:44:24
 */
public class DataAction extends ActionSupport {
	private static final long serialVersionUID = -84029134119170906L;
	private Map<String, Object> responseJson;

	private List<String> strs;

	public String getJson() {

		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < 3; i++) {
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("id", i);
			m.put("name", "苹果Mac" + i);
			m.put("age", 20);
			m.put("null", null);
			list.add(m);
		}
		map.put("rows", list);
		map.put("totalCont", 3);
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		try {
			response.getWriter().println(JSONUtil.serialize(map));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getJson1() {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < 3; i++) {
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("id", i);
			m.put("name", "苹果" + i);
			m.put("age", 20);
			m.put("null", null);
			list.add(m);
		}
		map.put("rows", list);
		map.put("totalCont", 3);
		this.setResponseJson(map);
		List<String> s = new ArrayList<String>();
		s.add("liuxing");
		s.add("shaohan");
		this.setStrs(s);

		return SUCCESS;
	}

	public Map<String, Object> getResponseJson() {
		return responseJson;
	}

	public void setResponseJson(Map<String, Object> responseJson) {
		this.responseJson = responseJson;
	}

	public List<String> getStrs() {
		return strs;
	}

	public void setStrs(List<String> strs) {
		this.strs = strs;
	}

}
