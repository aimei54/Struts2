package com.firewolf.struts2.actions;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

public class TestRequestWayAction{
	public String execute(){
		
		Map<String,Object> requestMap = (Map<String, Object>) ActionContext.getContext().getParameters();
		String param = ((String[])requestMap.get("param"))[0];
		int a = Integer.parseInt(param);
		if(a == 1)
			return "default";
		else if(a==2){
			return "dispatcher";
		}else if(a==3){
			return "redirect";
		}else if(a==4){
			return "redirectAction";
		}else if(a==5){
			return "chain";
		}else if(a==6){
			return "redirectAction2";
		}else if(a==7){
			return "redirectAction3";
		}else if(a==8){
			return "chain2";
		}else{
			return "index";
		}
	}
}
