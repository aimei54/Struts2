package com.firewolf.struts.typeconverse;


import java.util.Date;

public class InputAction {
	private Date birthDay;

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	
	public String execute(){
		System.out.println(birthDay.toGMTString());
		return "success";
	}
}
