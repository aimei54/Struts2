package com.firewolf.struts.typeconverse;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

public class StringDateConvertor extends StrutsTypeConverter{

	private DateFormat format;
	
	public StringDateConvertor() {
		
//		ServletContext context = ServletActionContext.getServletContext();
//		String patten = context.getInitParameter("patten");
//		format = new SimpleDateFormat(patten);
		format = new SimpleDateFormat("yyyy,MM,dd");
		System.out.println("实例化");
		
	}
	
	
	@Override
	public Object convertFromString(Map arg0, String[] arg1, Class arg2) {
		System.out.println("转成对象");
		if(arg1 != null && arg1.length>0){
			String str = arg1[0];
			if(arg2 == Date.class){
				try {
					return format.parse(str);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	public String convertToString(Map arg0, Object arg1) {
		if(arg1 instanceof Date){
			return format.format(arg1);
		}
		return null;
	}

}
