package com.firewolf.struts2.actions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.util.ServletContextAware;

/**
 * 实现下面这一组接口中的某一些，并实现方法来接受传递过来的参数，来获取到Servlet API
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月4日
 * @package : com.firewolf.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月4日下午2:50:47
 */
public class TestServletAwareAction implements ServletRequestAware, ServletResponseAware, ServletContextAware {

	private HttpServletRequest request;

	private HttpServletResponse response;

	private ServletContext context;

	public String execute() {
		request.setAttribute("requestKey", "requestValue4");
		return "ok";
	}

	//	@Override
	public void setServletContext(ServletContext arg0) {
		this.context = arg0;
	}

	//	@Override
	public void setServletResponse(HttpServletResponse arg0) {
		this.response = arg0;
	}

	//	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request = arg0;
	}

}
