package com.firewolf.struts2.actions;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

/**
 * 使用ServletActionContext解耦的方式访问资源
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月4日
 * @package : com.firewolf.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月4日下午2:21:21
 */
public class TestServletActionContext {

	public String execute(){
		
		/**
		 * 可以使用下面的一些方式来获取一些和Servlet API相关的一些对象
		 */
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		HttpServletResponse response = ServletActionContext.getResponse();
		ServletContext servletContext = ServletActionContext.getServletContext();
	
		request.setAttribute("requestKey", "requestValue3");
		return "ok";
	}
}
