package com.firewolf.struts2.actions;

import java.util.Map;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;

/**
 * 实现下面这一组接口中的某一些，并实现方法来接受传递过来的参数，来获取到Servlet API
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月4日
 * @package : com.firewolf.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月4日下午2:50:47
 */
public class TestAwareAction implements ApplicationAware,SessionAware,RequestAware,ParameterAware{

	//定义这些对象来接受
	private Map<String, Object> applicationMap;
	
	//sessionMap其实对应的类型是SessionMap类型的
	private Map<String, Object> sessionMap;
	private Map<String, Object> requestMap;
	//这个参数的map加进去的数据不会有效，
	private Map<String, String[]>  paramterMap;
	
	public String execute(){
		applicationMap.put("applicationKey", "application value2");
		sessionMap.put("sessionKey", "session value2");
		requestMap.put("requestKey", "request value2");
		System.out.println(paramterMap.get("param1")[0]);
		
		//可以通过调用下面的方式让session失效
		if(sessionMap instanceof SessionMap){
			SessionMap smap = (SessionMap) sessionMap;
			smap.invalidate();
		}
		
		return "ok";
	}

//	@Override
	public void setApplication(Map<String, Object> arg0) {
		this.applicationMap = arg0;
	}

//	@Override
	public void setRequest(Map<String, Object> arg0) {
		this.requestMap = arg0;
	}

//	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sessionMap = arg0;
	}

//	@Override
	public void setParameters(Map<String, String[]> arg0) {
		this.paramterMap = arg0;
	}
	
}
