package com.firewolf.struts2.actions;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

/**
 * 使用ActionContext解耦的方式访问资源
 * @Description:
 * @Author: 刘兴
 * @Date : 2016年9月4日
 * @package : com.firewolf.struts2.actions
 * @Java_Version : 1.7
 * @Version : 2016年9月4日下午2:21:21
 */
public class TestActionContext {

	public String execute(){
		
		
		//0.获取ActionContext对象
		//ActionContext对象是上下文，可以获取到很多信息
		ActionContext context = ActionContext.getContext();
		
		//使用Application
		//1.获取application对象的map，并向其中添加一个属性
		Map<String, Object> applicationMap = context.getApplication();
		applicationMap.put("applicationKey", "one application");
		//获取属性
		Object object = applicationMap.get("date");
		System.out.println(object);
		
		//使用Session
		Map<String, Object> sessionMap = context.getSession();
		sessionMap.put("sessionKey", "one session");
		
		//获取Request，这个方式有点不同
		Map<String, Object> requestMap = (Map<String, Object>) context.get("request");
		requestMap.put("requestKey", "one request");
		
		//获取请求参数
		Map<String, Object> parameters = context.getParameters();
		//为什么是数组？比喻说复选框这种，对应的是多个值。
		String[] object2 = (String[]) parameters.get("param1");
		System.out.println(object2[0]);
		
		return "ok";
	}
}
