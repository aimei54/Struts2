<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h1>访问Servlet API的方式</h1>
	<%
		application.setAttribute("date", "2013.07.12");
	%>
	<a href="testActionContext.action?param1=value1">ActionContext解耦方式</a><br>
	<a href="testAwareAction.action?param1=value1">XxxAware解耦方式</a><br>
	<a href="testServletActionContext.action?param1=value1">ServletActionContext耦合方式</a><br>
	<a href="testServletAwareAction.action?param1=value1">ServletXxxAware耦合方式</a><br>
</body>
</html>